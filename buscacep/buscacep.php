<?php
header("Content-type: text/html; charset=utf-8"); 
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: *");
ini_set('magic_quotes_runtime', 0);

try {

    $json = file_get_contents('php://input');
    $obj = json_decode($json, TRUE);
        
    $retorno = array();

    $dadosCep = file_get_contents('https://viacep.com.br/ws/'.$obj['cep'].'/json/');

    if(!is_null($dadosCep)){
        $dadosCep = json_decode($dadosCep, true);
    
        $retorno = [
            'zipcode'      => $obj['cep'],
            'street'       => $dadosCep['logradouro'],
            'neighborhood' => $dadosCep['bairro'],
            'city'         => $dadosCep['localidade'],
            'state'        => $dadosCep['uf'],
        ];
    } else {
        throw new Exception('CEP não localizado');
    }

    echo json_encode($retorno,JSON_UNESCAPED_UNICODE);
} catch (Exception $e) {
    echo $e->getMessage();
}
