<?php

$estadosBrasileiros = array(
    'AC'=>'Acre',
    'AL'=>'Alagoas',
    'AP'=>'Amapá',
    'AM'=>'Amazonas',
    'BA'=>'Bahia',
    'CE'=>'Ceará',
    'DF'=>'Distrito Federal',
    'ES'=>'Espírito Santo',
    'GO'=>'Goiás',
    'MA'=>'Maranhão',
    'MT'=>'Mato Grosso',
    'MS'=>'Mato Grosso do Sul',
    'MG'=>'Minas Gerais',
    'PA'=>'Pará',
    'PB'=>'Paraíba',
    'PR'=>'Paraná',
    'PE'=>'Pernambuco',
    'PI'=>'Piauí',
    'RJ'=>'Rio de Janeiro',
    'RN'=>'Rio Grande do Norte',
    'RS'=>'Rio Grande do Sul',
    'RO'=>'Rondônia',
    'RR'=>'Roraima',
    'SC'=>'Santa Catarina',
    'SP'=>'São Paulo',
    'SE'=>'Sergipe',
    'TO'=>'Tocantins'
);

function geraUFComboHTML($nameHTML, $required='', $myUF = "") {
    global $estadosBrasileiros;

    $options = " <select class='form-control' id='$nameHTML' name='$nameHTML' $required>
    <option value=''>UF - Selecione </option>";
    

    foreach ($estadosBrasileiros as $uf => $estado) {
        $selected  = $uf == $myUF ?"selected":"";
        $options  .= "<option $selected value=\"$uf\">$uf - $estado</option>";
    }
    
    $options .= "</select>"; 
        
    return $options;
}

function geraComboEstadoCivil($nameHTML, $required="") {
    
    $combo = '<select class="form-control" name="'.$nameHTML.'" id="'.$nameHTML.'">'.
            '<option value="Solteiro">Solteiro</option>'.
            '<option value="Casado">Casado</option>'.
            '<option value="Separado">Separado</option>'.
            '<option value="Divorciado">Divorciado</option>'.
            '<option value="Viúvo">Viúvo</option>'.
            '<option value="Amasiado">Amasiado</option>'.
            '<option value="União Estável">União Estável</option>'.
            '</select>';
    return $combo;
}

?>