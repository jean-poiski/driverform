<?php
    function getPaginaEmail(){
        return '
        <!DOCTYPE html>
        <html lang="pt-BR">
        
        <head>
          <meta charset="utf-8" />
          <meta http-equiv="X-UA-Compatible" content="IE=edge">
          <title>Cadastro de Entregadores / Parceiros </title>
          <meta name="viewport" content="width=device-width, initial-scale=1">
        
          <!-- Styles -->
          <link rel="stylesheet" type="text/css" media="screen" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" />
          <link rel="stylesheet" type="text/css" media="screen" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" />
          <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" />
          <link rel="stylesheet" type="text/css" media="screen" href="../css/main.css" />
        
          <!-- Scripts --> 
          <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
          <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.3.4/jquery.inputmask.bundle.js"></script>
          <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
          <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
          <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.bundle.min.js"></script>
          <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>

          </head>

          <body>
          
          <div class="container">
              
            <div class="jumbotron jumbotron-fluid qargo-cabecalho">
            <div class="row">
              
              <div class="col-md-3 col-sm-6">
                <div >
                  <img src="../img/logo-original-horizonta.png" class="img-responsive qargo-cabecalho mt-4" width="100%"></img>
                </div>
              </div>
              <div class="col-md-9 col-sm-6">
      
                <h3 class="text-center qargo-cabecalho-title">E-mail enviado com Sucesso!</h3>  
      
                <span class="lead text-muted text-center qargo-cabecalho-text">
                  <p>Seu cadastro foi enviado com sucesso, e será analisado por nossa gerenciadora de riscos!</p>

                  <p>Assim que obtivermos o retorno da análise, retomaremos o contato no e-mail informado no formulário. <i class="far fa-smile-wink"></i>  </p> 
                </span>
        
              </div>
      
            </div>
      
          </div>
      
        </body>';
    }

?>