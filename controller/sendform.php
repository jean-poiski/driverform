<?php

require_once "phpmailer/class.phpmailer.php";
require_once "emailenviado.php";

define('GUSER', 'driverformqargo2018@gmail.com');	// <-- Insira aqui o seu GMail
define('GPWD', 'form@123');		// <-- Insira aqui a senha do seu GMail

$error = "";

function getDescricaoCampo($campo) {

    $camposForm = array(
        "nrcpf"=> "CPF",
        "nomemotorista"=> "Nome",
        "nrrg"=> "RG",
        "dtemissaorg"=> "Emissão RG",
        "ufemissaorg"=> "Estado Emissão RG",
        "orgaoemissaorg"=> "Orgão Emissor RG",
        "dtnasc"=> "Data de Nascimento",
        "dsufnatural"=> "Estado Naturalidade",
        "dscidadenatural"=> "Cidade Naturalidade",
        "estadocivil"=> "Estado Civil",
        "celular"=> "Nº Celular",
        "dsemail"=> "E-mail",
        "telefone"=> "Nº Telefone",
        "pessoacontato"=> "Pessoa de contato",
        "telefoneref"=> "Nº Telefone Referência",
        "pessoacontatoref"=> "Pessoa de contato como Referência",
        "nrcep"=> "CEP",
        "dsrua"=> "Rua",
        "dsnumero"=> "Número",
        "dsbairro"=> "Bairro",
        "dscomp"=> "Complemento",
        "dsuf"=> "Estado",
        "dscidade"=> "Cidade",
        "nomepai"=> "Nome do Pai",
        "nomemae"=> "Nome da Mãe",
        "profissao"=> "Profissão",
        "nrcnh"=> "Nº CNH",
        "nrregcnh"=> "Nº Registro CNH",
        "categoriacnh"=> "Categoria CNH",
        "dtvenccnh"=> "Data de Validade CNH",
        "dtprimeiracnh"=> "Data da Primeira CNH",
        "dtemissaocnh"=> "Data de Emissão CNH",
        "ufcnh"=> "Estado Emissão CNH",
        "orgaocnh"=> "Órgão Emissor CNH",
        "segurancacnh"=> "Código de Segurança",
        "nrplaca"=> "Placa Veículo",
        "vrenavam"=> "RENAVAM",
        "vchassi"=> "Chassi",
        "vmodelo"=> "Modelo/Marca",
        "vcidadeuf"=> "Cidade e Estado Emissor",
        "vano"=> "Ano Fabricação", 
        "vanomodelo"=> "Ano Modelo",
        "vcor"=> "Cor",
        "vdtemissao"=> "Emissão Documento Veículo",
        "vcombustivel"=> "Tipo Combustível",
        "vtipocarroceria"=>"Tipo Carroceria",
        "vrntrc"=>"Nº RNTRC",
        "vdtvencrntrc"=>"Validade RNTRC",
        'propnrcnpj' => "CNPJ Proprietário",
        'proprazaosocial' => "Razão Social Proprietário",
        'propnrcpf' => "CPF Proprietário",
        'propnome' => "Nome Proprietário",
        'propnrrg' => "RG Proprietário",
        'propdtemissaorg' => "Data Emissão RG Proprietário",
        'propufemissaorg' => "UF Emissão RG Proprietário",
        'proporgaoemissaorg' => "Órgão emissor RG Proprietário",
        'propdtnasc' => "Data Nascimento Proprietário",
        'propdsufnatural' => "UF Naturalidade Proprietário",
        'propdscidadenatural' => "Cidade Naturalidade Proprietário",
        'propnrcep' => "CEP Proprietário",
        'propdsrua' => "Rua Proprietário",
        'propdsnumero' => "Nº Residência Proprietário",
        'propdsbairro' => "Bairro Proprietário",
        'propdscomp'=> "Complemento Endereço Proprietário",
        'propdsuf'=> "UF Proprietário",
        'propdscidade' => "Cidade Proprietário",
        'propcelular'=> "Celular Proprietário",
        'proptelefone'=> "Telefone Proprietário",
        'proppessoacontato'=> "Pessoa de Contato do Proprietário",
        'proptelefoneref' => "Telefone de referência do Proprietário",
        'proppessoacontatoref' => "Pessoa de referência do Proprietário",
        'propnomepai' => "Pai do Proprietário",
        "propnomemae" => "Mãe do Proprietário"
    );

    return $camposForm[$campo];
}

function enviarEmail() {

    $data_envio = date('d/m/Y');
    $hora_envio = date('H:i:s');

    $corpoEmail = "
    <html>
    <head>
    <meta charset='utf-8' />
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <title>Cadastro de Entregadores QArgo </title>
    <meta name='viewport' content='width=device-width, initial-scale=1'>

    <!-- Styles -->
    <link rel='stylesheet' type='text/css' media='screen' href='https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css' />
    <link rel='stylesheet' type='text/css' media='screen' href='https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css' />
    <link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.1.0/css/all.css' />
    <link rel='stylesheet' type='text/css' media='screen' href='http://qargo.com.br/driverform/driverform/css/main.css' />
  
    <!-- Scripts --> 
    <script src='https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js' integrity='sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49' crossorigin='anonymous'></script>
    <script src='https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js'></script>
    <script src='https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.bundle.min.js'></script>

    <style>
    .qargo-cabecalho {
        background-color: #250044 !important;  
        text-align: center;   
    }
   
    .qargo-cabecalho-title {
        color: #00f5ff !important;
        font-weight: bold;
        font-style: italic;
    }
   
    .qargo-cabecalho-text {
       color: white !important;
    }
    </style>

    </head>
    <body>
    <div class='container'>
              
    <div class='jumbotron jumbotron-fluid qargo-cabecalho'>
        
        <table width='100%' border='0' cellpadding='1' cellspacing='1' class='table'>
            <tr>
                <td>
                    <img src='http://qargo.com.br/driverform/driverform/img/logo-original-horizonta.png' class='img-responsive qargo-cabecalho mt-4' width='255px' heigth='113px'></img>
                </td>
                <td>
                    <h1 class='text-center qargo-cabecalho-title'>Obrigado!</h1>  

                    <span class='lead text-muted text-center qargo-cabecalho-text'>
                    <p>Aqui estão as informações preenchidas em nosso cadastro e anexos. Agradecemos o tempo dedicado na realização do cadastro! <i class='far fa-smile-wink'></i>  </p>
                    </span>
                </td>
            </tr>
        </table>

    </div>

    <br />

    <table width='100%' border='1' cellpadding='1' cellspacing='1' class='table table-bordered table-striped'>";

    
    foreach ($_POST as $key => $value) {

        if(!isset($value) || $value == null || trim($value) == "") {
            
            if($key == "propnrcnpj" || $key == "propnrcpf") {
                $corpoEmail .= "<tr>";
                $corpoEmail .= "<td>Motorista também é o Proprietário?</td>";
                $corpoEmail .= "<td>SIM</td>";
                $corpoEmail .= "</tr>";
            }
            
            continue;
        }
            
        $corpoEmail .= "<tr>";
        $corpoEmail .= "<td>".getDescricaoCampo($key)."</td>";
        
        if (($key == "dtemissaorg") || ($key ==  "dtnasc") || ($key == "dtvenccnh")
        ||  ($key == "dtprimeiracnh") || ($key == "dtemissaocnh") || ($key == "vdtemissao")
        || ($key == "vdtvencrntrc") || ($key == "propdtemissaorg") || ($key == "propdtnasc")) {

            $corpoEmail .= "<td>".date('d/m/Y',strtotime($value))."</td>";
        } else
            $corpoEmail .= "<td>$value</td>";
        
        $corpoEmail .= "</tr>";
    }
            
    $corpoEmail .= "</table>
            
    <p>Este e-mail foi enviado em <b>$data_envio</b> às <b>$hora_envio</b></p>
            
    </body>  
    </html>
    ";

	$mail = new PHPMailer(true);
	$mail->SMTPDebug = 1;		// Debugar: 1 = erros e mensagens, 2 = mensagens apenas
	$mail->SMTPAuth = true;		// Autenticação ativada
	$mail->SMTPSecure = 'ssl';	// SSL REQUERIDO pelo GMail
	$mail->Host = 'smtp.gmail.com';	// SMTP utilizado
	$mail->Port = 465;  		// A porta 587 deverá estar aberta em seu servidor
	$mail->Username = GUSER;
	$mail->Password = GPWD;
    $mail->Subject = "Cadastro de Entregadores Qargo";
	$mail->Body = $corpoEmail;
    $mail->CharSet = 'UTF-8';
    $mail->IsHTML();
	$mail->IsSMTP();		// Ativar SMTP
	$mail->SetFrom(GUSER, "Cadastro Entregadores Qargo");
    $mail->AddAddress('entregador@qargo.com.br');
    $mail->AddAddress(GUSER);

    if (isset($_POST['dsemail']) && trim($_POST['dsemail']) != "") 
        $mail->AddAddress($_POST['dsemail']);
    
    
    $anexos = array("arquivoCNH", "arquivoDocResidencia", "arquivoCNHProp", "arquivoDocResidenciaProp", "arquivoCRLV", "arquivoRNTRC");

    foreach ($anexos as $key) {
        if(isset($_FILES[$key])) {

            $arqAnexo = $_FILES[$key];
            $mail->AddAttachment($arqAnexo['tmp_name'], $arqAnexo['name']);

        }
    }

	if($mail->Send()) {
		return getPaginaEmail();
	} else {
		return "ERRO AO ENVIAR E-MAIL!";
	}
}

echo enviarEmail();

?>