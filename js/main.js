var idsPropJud = [
    'propnrcnpj',
    'proprazaosocial'
];

var idsPropFis = [
    'propnrcpf',
    'propnome',
    'propnrrg',
    'propdtemissaorg',
    'propufemissaorg',
    'proporgaoemissaorg',
    'propdtnasc',
    'propdsufnatural',
    'propdscidadenatural',
    'propnrcep',
    'propdsrua',
    'propdsnumero',
    'propdsbairro',
    'propdscomp',
    'propdsuf',
    'propdscidade',
    'propcelular',
    'proptelefone',
    'proppessoacontato',
    'proptelefoneref',
    'proppessoacontatoref',
    'propnomepai',
    "propnomemae",
    "arquivoCNHProp",
    "arquivoDocResidenciaProp"
];

var cidadesPermitidas = [];

function elemento( el ){
	return document.getElementById( el );
}

function setCamposPropJud(bRequired, bLimpar){
    for (i = 0; i < idsPropJud.length; i++) {
        elemento(idsPropJud[i]).required = bRequired;
        
        if(bLimpar)
            elemento(idsPropJud[i]).value = "";
    }
}

function setCamposPropFis(bRequired, bLimpar){
    for (i = 0; i < idsPropFis.length; i++) {
        
        if(idsPropFis[i] != 'propdscomp')
            elemento(idsPropFis[i]).required = bRequired;

        if(bLimpar)
            elemento(idsPropFis[i]).value = "";
    }

}

function isEmpty(obj) {
    return (obj == undefined || obj == null || obj == "");
}

function manipulaCamposVeiculoConsulta(vModelo, vAno, vCor, vMunUF) {
    
    var semInf = isEmpty(vModelo);
    
    elemento("vmodelo").value = vModelo;
    elemento("vano").value = vAno;
    elemento("vanomodelo").value =vAno;
    elemento("vcor").value = vCor;
    elemento("vcidadeuf").value = vMunUF;

    elemento("vmodelo").readOnly = !semInf;
    elemento("vano").readOnly = !semInf;
    elemento("vanomodelo").readOnly = !semInf;
    elemento("vcor").readOnly = !semInf;
    elemento("vcidadeuf").readOnly = !semInf;

    //Indica que a consulta não retornou os dados do veículo
    toastr[semInf ? "warning" : "success"](semInf ? 'Não conseguimos encontrar seu veículo. Por favor, preencha as demais informações manualmente' : 'Encontramos seu veículo :)');

}

function carregaDadosVeiculo(){
    
    var placa = elemento('nrplaca').value;

    if(isEmpty(placa))
        return;
    

    $('#linha-dados-veic').loading({
        message: '<i class="fa fa-car faa-passing animated"></i> Estamos localizando seu veículo',
        theme: 'dark'
    });

    $.ajax({
        type: "POST",
        url: "../driverform/sinesp/placa.php",
        data: JSON.stringify({placa:placa}),
        success: function(res){
            
            $('#linha-dados-veic').loading('toggle');

            if(!isEmpty(res) && !isEmpty(res.modelo)) {
                manipulaCamposVeiculoConsulta(res.modelo, res.ano, res.cor, res.municipio+" - "+res.uf);
            } else {
                manipulaCamposVeiculoConsulta("", "", "", "");
            }
            console.info(JSON.stringify(res));

        },
        error: function(err){
            $('#linha-dados-veic').loading('toggle');

            manipulaCamposVeiculoConsulta("", "", "", "");

            console.error(JSON.stringify(err));
        },
        dataType: "json",
        contentType : "application/json"
      });		
}

function carregaDadosCep(el){
    
    var cep = el.value;

    var cProp = (el.id.indexOf("prop") == -1 ? "" : "prop");

    if(cep == null || cep == undefined || cep == "")
        return;

    cep = cep.replace(".","");
    cep = cep.replace("-","");
    
    elemento(cProp+"dsrua").disabled = true;
    elemento(cProp+"dsbairro").disabled = true;
    elemento(cProp+"dscidade").disabled = true;
    elemento(cProp+"dsuf").disabled = true;

    $.ajax({
        type: "POST",
        url: "../driverform/buscacep/buscacep.php",
        data: JSON.stringify({cep:cep}),
        dataType: "json",
        contentType : "application/json",
        success: function(res){

            if(cProp == "" && cidadesPermitidas.indexOf(res.city.toLowerCase()) <= -1){
                elemento(cProp+"nrcep").value = "";
                elemento(cProp+"dsrua").value = "";
                elemento(cProp+"dsbairro").value = "";
                elemento(cProp+"dsuf").value = "";
                elemento(cProp+"dscidade").value = "";

                elemento(cProp+"dsrua").disabled = false;
                elemento(cProp+"dsbairro").disabled = false;
                elemento(cProp+"dscidade").disabled = false;
                elemento(cProp+"dsuf").disabled = false;

                toastr.warning('Temporariamente disponível para residentes na Capital e Região Metropolitana de São Paulo');
                return;
            }

            elemento(cProp+"dsrua").value = res.street;
            elemento(cProp+"dsbairro").value = res.neighborhood;
            elemento(cProp+"dsuf").value = res.state;
            
            $("#"+cProp+"dsuf").change();

            elemento(cProp+"dscidade").value = res.city;

            toastr.success('Encontramos o Endereço :)');

            elemento(cProp+"dsrua").disabled = false;
            elemento(cProp+"dsbairro").disabled = false;
            elemento(cProp+"dscidade").disabled = false;
            elemento(cProp+"dsuf").disabled = false;

            elemento("dsnumero").focus();

        },
        error: function(err){
            toastr.error('Não encontramos o endereço. O CEP está correto? :( ')
            elemento(cProp+"dsrua").disabled = false;
            elemento(cProp+"dsbairro").disabled = false;
            elemento(cProp+"dscidade").disabled = false;
            elemento(cProp+"dsuf").disabled = false;
            console.error(JSON.stringify(err));
        }
    });		
}

function carregaCidadesPermitidas() {
    try {
        $.getJSON('cidades_permitidas.json', function (data) {
            cidadesPermitidas = data;        
        });            
    } catch (error) {
        console.error(error);
    }    
}

function carregaCidadesEstados(idEstado, idCidade){

    try {
        $.getJSON('estados_cidades.json', function (data) {
            var items = [];
            var options = '<option value="">UF - Selecione</option>';	
            $.each(data, function (key, val) {
                options += '<option value="' + val.sigla + '">' + val.sigla + " - "+ val.nome + '</option>';
            });					
            $("#"+idEstado).html(options);				
            
            $("#"+idEstado).change(function () {				
            
                var options_cidades = '';
                var str = "";					
                
                $("#"+idEstado+" option:selected").each(function () {
                    str += $(this).text();
                });
                
                $.each(data, function (key, val) {
                    if(val.nome == str || val.sigla+" - "+val.nome == str) {
                        $.each(val.cidades, function (key_city, val_city) {
                            options_cidades += '<option value="' + val_city + '">' + val_city + '</option>';
                        });							
                    }
                });
                
                if(idCidade != null && idCidade != undefined && idCidade != "")
                    $("#"+idCidade).html(options_cidades);
                
            }).change();		
        
        });            
    } catch (error) {
        console.error(error);
    }
}

function toolTipCidades(el) {
    var temCidade = false;
    $('#'+el.id +' option').each(function(){
        var opcao = $(this).val();
        if(!temCidade && opcao != null && opcao != undefined && opcao != "")
            temCidade = true;
    });

    if(!temCidade)
        $('#'+el.id).attr('title', 'Para escolher a cidade, escolha sua UF primeiro').tooltip('show');
    else
        $('#'+el.id).attr('title', '').tooltip('dispose');
}

//Verifica se CPF é válido
function validaCPF(campoCPF) {

    $('#'+campoCPF.id).attr('title', '').tooltip('dispose');

    if((campoCPF.value != undefined && campoCPF.value != null && campoCPF.value != "") && (!TestaCPF(campoCPF.value))) {
        $('#'+campoCPF.id).attr('title', 'CPF inválido :(').tooltip('show');
        campoCPF.focus();
    }
}

//Verifica se CNPJ é válido
function validaCNPJ(campoCNPJ) {

    $('#'+campoCNPJ.id).attr('title', '').tooltip('dispose');

    if((campoCNPJ.value != undefined && campoCNPJ.value != null && campoCNPJ.value != "") && (!TestaCNPJ(campoCNPJ.value))) {
        $('#'+campoCNPJ.id).attr('title', 'CNPJ inválido :(').tooltip('show');
        campoCNPJ.focus();
    }
}

function TestaCPF(strCPF) {
    var Soma;
    var Resto;
    Soma = 0;   
    
    while(strCPF.indexOf(".") > 0)
        strCPF = strCPF.replace(".","");

    strCPF = strCPF.replace("-","");

    if (strCPF == "00000000000")
        return false;
    
    for (i=1; i<=9; i++)
        Soma = Soma + parseInt(strCPF.substring(i-1, i)) * (11 - i); 
        
    Resto = (Soma * 10) % 11;
    if ((Resto == 10) || (Resto == 11)) 
	    Resto = 0;
    if (Resto != parseInt(strCPF.substring(9, 10)) )
	    return false;
    Soma = 0;
    
    for (i = 1; i <= 10; i++)
       Soma = Soma + parseInt(strCPF.substring(i-1, i)) * (12 - i);
    Resto = (Soma * 10) % 11;
    
    if ((Resto == 10) || (Resto == 11)) 
	    Resto = 0;
    
    if (Resto != parseInt(strCPF.substring(10, 11) ) )
        return false;
    return true;
}

function TestaCNPJ(cnpj) {
 
    cnpj = cnpj.replace(/[^\d]+/g,'');
 
    if(cnpj == '') return false;
     
    if (cnpj.length != 14)
        return false;
 
    // Elimina CNPJs invalidos conhecidos
    if (cnpj == "00000000000000" || 
        cnpj == "11111111111111" || 
        cnpj == "22222222222222" || 
        cnpj == "33333333333333" || 
        cnpj == "44444444444444" || 
        cnpj == "55555555555555" || 
        cnpj == "66666666666666" || 
        cnpj == "77777777777777" || 
        cnpj == "88888888888888" || 
        cnpj == "99999999999999")
        return false;
         
    // Valida DVs
    tamanho = cnpj.length - 2
    numeros = cnpj.substring(0,tamanho);
    digitos = cnpj.substring(tamanho);
    soma = 0;
    pos = tamanho - 7;
    for (i = tamanho; i >= 1; i--) {
      soma += numeros.charAt(tamanho - i) * pos--;
      if (pos < 2)
            pos = 9;
    }
    resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
    if (resultado != digitos.charAt(0))
        return false;
         
    tamanho = tamanho + 1;
    numeros = cnpj.substring(0,tamanho);
    soma = 0;
    pos = tamanho - 7;
    for (i = tamanho; i >= 1; i--) {
      soma += numeros.charAt(tamanho - i) * pos--;
      if (pos < 2)
            pos = 9;
    }
    resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
    if (resultado != digitos.charAt(1))
          return false;
           
    return true;
    
}

function setCamposObrigatoriosProprietario(idPill){

    setCamposPropJud((idPill == 'pills-propjuridica-tab' || idPill == "pills-propnao-tab"), true);
    setCamposPropFis(idPill == "pills-propfisica-tab", idPill != "pills-propfisica-tab");


    if(idPill == "pills-propsim-tab" || idPill == 'pills-propjuridica-tab' || idPill == "pills-propfisica-tab") {
        
        elemento('card-docs-prop').disabled = (idPill != "pills-propfisica-tab");
        
        switch (idPill) {
            case "pills-propjuridica-tab":
                elemento('card-docs-prop-title').innerHTML = "Não é necessário informar, pois o proprietário é Pessoa Jurídica";
                break;
            default:
                elemento('card-docs-prop-title').innerHTML = (idPill != "pills-propfisica-tab" ? "Não é necessário informar, pois o motorista é o Proprietário": "Proprietário");
                break;
        }

        if(elemento('card-docs-prop').disabled) {
            $("#LbarquivoCNHProp").removeClass("required");
            $("#LbarquivoDocResidenciaProp").removeClass("required")
        } else {
            $("#LbarquivoCNHProp").addClass("required");
            $("#LbarquivoDocResidenciaProp").addClass("required")
        }
    }
    


}

function validarCamposForm(){
    var formValido = true;

    $("#campos-obrigatorios").removeClass('show');

    formValido = document.forms["principal"].checkValidity();

    if(!formValido)
        $("#campos-obrigatorios").addClass('show');
    else
        document.forms["principal"].submit();

    return formValido;
}

function trocarGuias(guiaDestino) {
    $('#'+guiaDestino).trigger('click');
}

/* Máscaras ER */
function mascara(o,f){
    v_obj=o
    v_fun=f
    setTimeout("execmascara()",1)
}
function execmascara(){
    v_obj.value=v_fun(v_obj.value)
}
function mtel(v){
    v=v.replace(/\D/g,"");             //Remove tudo o que não é dígito
    v=v.replace(/^(\d{2})(\d)/g,"($1) $2"); //Coloca parênteses em volta dos dois primeiros dígitos
    v=v.replace(/(\d)(\d{4})$/,"$1-$2");    //Coloca hífen entre o quarto e o quinto dígitos
    return v;
}

//Onload principal
window.onload = function(){
    elemento('telefone').onkeyup = function(){
		mascara( this, mtel );
    };
    
    elemento('celular').onkeyup = function(){
		mascara( this, mtel );
    };
    
    elemento('telefoneref').onkeyup = function(){
		mascara( this, mtel );
    };

    elemento('proptelefone').onkeyup = function(){
		mascara( this, mtel );
    };
    
    elemento('propcelular').onkeyup = function(){
		mascara( this, mtel );
    };
    
    elemento('proptelefoneref').onkeyup = function(){
		mascara( this, mtel );
    };


}