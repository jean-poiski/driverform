<?php
header("Content-type: application/json; charset=utf-8"); 
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: *");
ini_set('magic_quotes_runtime', 0);
require 'vendor/autoload.php';

use Sinesp\Sinesp;

try {
    $proxyList = array(
        '201.57.97.114'=>'3128',
        '200.195.162.170'=>'8888',
        '177.85.86.11'=>'8080',
        '186.200.35.147'=>'3128',
        '191.252.200.100'=>'3128',
        '191.252.185.161'=>'8090',
        '191.37.201.226'=>'8080',
        '189.39.141.174'=>'20183',
        '189.7.97.54'=>'8080',
        '187.22.2.160'=>'20183',
        '177.72.56.155'=>'3128',
        '177.91.201.178'=>'8080',
        '201.57.97.114'=>'3128',
        '200.195.162.170'=>'8888',
        '177.85.86.11'=>'8080'
    );

    $endProxy = "0";
    $portaPoxy = "0";
    
    foreach ($proxyList as $end => $port) {
        $conectado = @ fsockopen($end, $port, $numeroDoErro, $stringDoErro, 7);
        if($conectado) {
            $endProxy = $end;
            $portaPoxy =$port;
            break;
        }
    }

    $veiculo = new Sinesp;

    $json = file_get_contents('php://input');
    $obj = json_decode($json, TRUE);

    if($endProxy != '0')
        $veiculo->proxy($endProxy, $portaPoxy);

    $veiculo->buscar($obj['placa']);

    if ($veiculo->existe()) {
        echo json_encode($veiculo->dados(),JSON_UNESCAPED_UNICODE);        
    }
} catch (Exception $e) {
    echo $e->getMessage();
}